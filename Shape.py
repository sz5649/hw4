import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as pth
import matplotlib.patches as patches

from Transform import *
#Base class for shapes (which will )
#All shapes are defined w.r.t. a link (parent jonit) frame, 
#with an initial (static) rigid body transform (T0) relative to that frame 
#IMPROVE: make this an abstract base class
class Shape:
	def __init__(self,name_,transform_=Transform(np.zeros(2),0.0)):
		self.name = name_
		self.T0 = transform_

	def getStaticTransform(self):
		return self.T0

	#update internal variables from a transform object
	#baseTransform maps the parent frame to the inertial frame
	def update(self, baseTransform_=Transform(np.zeros(2),0.0)):
		#updates internal variables in preparation for drawing a figure
		print "UPDATE NOT IMPLEMENTED"
		pass

	#update internal variables from a transform represented as a matrix
	#baseTransform maps the parent frame to the inertial frame
	def updateFromMatrix(self, baseTMatrix_):
		#updates internal variables in preparation for drawing a figure
		print "UPDATE FROM MATRIX NOT IMPLEMENTED"
		pass

	#draw adds the object (e.g. a patch) to the axis ax_
	def draw(self, ax_):
		#draws the object
		print "DRAW NOT IMPLEMENTED"
		pass

#Line from p1 to p2
class Line(Shape):
	def __init__(self,name_,p1_,p2_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		#initialize member data
		self.p1 = p1_
		self.p2 = p2_
		#initial vertices
		self.v0 = [p1_,p2_]
		#fully transformed vertices
		self.vertices = [p1_,p2_]
		self.update()

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		baseT = baseTransform_.getInverseSE2()
		self.updateFromMatrix(baseT)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		self.vertices[0] = np.dot(T0[0:2,0:2],self.p1) + T0[0:2,2]
		self.vertices[1] = np.dot(T0[0:2,0:2],self.p2) + T0[0:2,2]

	def draw(self, ax_, color_='r',lw_=2):
		#Note: use of patch not completely necessary...but useful hint for rectangle implementation
		vtemp = self.vertices[:]
		codes = [pth.Path.MOVETO,pth.Path.LINETO]
		path = pth.Path(vtemp,codes)
		patch = patches.PathPatch(path,facecolor=color_,lw=lw_)
		ax_.add_patch(patch)

#Rectangle centered at (0,0) with width in x-direction and height in y-direction
class Rectangle(Shape):
	def __init__(self,name_,width_,height_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		self.width = width_
		self.height = height_
		self.v0 = [np.array([width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,-height_/2.0]),
						 np.array([width_/2.0,-height_/2.0])]
		self.vertices = [np.array([width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,-height_/2.0]),
						 np.array([width_/2.0,-height_/2.0])]
		# vertices in the form [x1, y1; x2, y2; x3, y3; x4, y4]
		self.update()

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		baseT = baseTransform_.getInverseSE2()
		self.updateFromMatrix(baseT)

	def updateFromMatrix(self, baseTMatrix_):
		# Calculate net transform T0
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		# Update vertices
		# for each vertex in self.v0, apply T0 to get a vertex in self.vertices
		for ii in range(0, len(self.v0)):
			self.vertices[ii] = np.dot(T0[0:2,0:2],self.v0[ii]) + T0[0:2,2]

	def draw(self, ax_, color_='r',lw_=2):
		# can refer to http://matplotlib.org/users/path_tutorial.html
		vx = self.vertices[:]
		vx.append(np.array([0, 0]))
		codes = [pth.Path.MOVETO, pth.Path.LINETO, pth.Path.LINETO, pth.Path.LINETO, pth.Path.CLOSEPOLY]
		path = pth.Path(vx, codes)
		patch = patches.PathPatch(path, facecolor = color_, lw = lw_)
		ax_.add_patch(patch)



#Circle of radius radius_ centered at (0,0)
class Circle(Shape):
	def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		self.radius = radius_
		self.center = np.zeros(2)
		self.update()

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		baseT = baseTransform_.getInverseSE2()
		self.updateFromMatrix(baseT)

	def updateFromMatrix(self, baseTMatrix_):
		# Calculate net transform T0 and apply it to the center of the circle
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		# is this correct?
		self.center[:] = T0[0:2, 2]

	def draw(self,ax_,color_='r'):
		# create a circular patch and add it ax_
		circle = plt.Circle((self.center[0], self.center[1]), radius = self.radius, color = color_)
		# TODO: artist or patch??
		ax_.add_artist(circle)
